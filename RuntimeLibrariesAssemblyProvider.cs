﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyModel;


namespace XploRe.Runtime.Loader
{

    // The default dependency context that is used by this provider is only available in .NETStandard 1.6 and newer.
#if !NETSTANDARD1_5

    /// <inheritdoc />
    /// <summary>
    ///     Provides assemblies for a collection of <see cref="T:Microsoft.Extensions.DependencyModel.RuntimeLibrary" /> 
    ///     instances.
    /// </summary>
    /// <remarks>
    ///     This provider is only available for .NET Standard 1.6 or newer.
    /// </remarks>
    public class RuntimeLibrariesAssemblyProvider : AssemblyNameCollectionAssemblyProvider
    {

        /// <summary>
        ///     Provides assemblies for all runtime libraries of the default <see cref="DependencyContext" /> instance.
        /// </summary>
        /// <remarks>
        ///     As the runtime libraries are retrieved from the default <see cref="DependencyContext" />, runtime 
        ///     libraries are only available if the ".deps.json" dependency information file for the entry assembly 
        ///     returned by <see cref="Assembly.GetEntryAssembly" /> is available. Otherwise, the list of runtime 
        ///     libraries is empty. 
        /// </remarks>
        [NotNull]
        public static RuntimeLibrariesAssemblyProvider Default
        {
            get {
                var libraries = DependencyContext.Default?.RuntimeLibraries ?? new List<RuntimeLibrary>(0);

                return new RuntimeLibrariesAssemblyProvider(libraries);
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.Loader.RuntimeLibrariesAssemblyProvider" /> instance
        ///     with a given collection of <see cref="T:Microsoft.Extensions.DependencyModel.RuntimeLibrary" />
        ///     instances.
        /// </summary>
        /// <param name="runtimeLibraries">
        ///     The collection of <see cref="T:Microsoft.Extensions.DependencyModel.RuntimeLibrary" /> instances to 
        ///     initialise the provider with.
        /// </param>
        /// <param name="assemblyLoadContext">
        ///     The <see cref="T:System.Runtime.Loader.AssemblyLoadContext" /> used to load assemblies. If not provided
        ///     or <c>null</c>, the <see cref="P:System.Runtime.Loader.AssemblyLoadContext.Default" /> context instance
        ///     is used instead.
        /// </param>
        public RuntimeLibrariesAssemblyProvider(
            [NotNull] [ItemNotNull] IEnumerable<RuntimeLibrary> runtimeLibraries,
            AssemblyLoadContext assemblyLoadContext = null)
            : base(runtimeLibraries.Select(library => new AssemblyName(library.Name)), assemblyLoadContext)
        {
        }

    }

#endif

}
