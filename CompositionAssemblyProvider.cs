﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;


namespace XploRe.Runtime.Loader
{

    /// <inheritdoc />
    /// <summary>
    ///     Composes a new <see cref="T:XploRe.Reflection.Loader.IAssemblyProvider" /> instance from existing providers. The resulting set of
    ///     candiates does not contain duplicates.
    /// </summary>
    public class CompositionAssemblyProvider : IAssemblyProvider
    {

        /// <summary>
        ///     The collection of <see cref="IAssemblyProvider" /> instances this provider is composed of.
        /// </summary>
        [NotNull]
        [ItemNotNull]
        public ICollection<IAssemblyProvider> AssemblyProviders { get; }

        /// <summary>
        ///     Initialises a new <see cref="CompositionAssemblyProvider" /> instance based on two providers.
        /// </summary>
        /// <param name="provider1">First <see cref="IAssemblyProvider" /> instance.</param>
        /// <param name="provider2">Second <see cref="IAssemblyProvider" /> intdance.</param>
        public CompositionAssemblyProvider(
            [NotNull] IAssemblyProvider provider1,
            [NotNull] IAssemblyProvider provider2)
        {
            if (provider1 == null) {
                throw new ArgumentNullException(nameof(provider1));
            }

            if (provider2 == null) {
                throw new ArgumentNullException(nameof(provider2));
            }

            AssemblyProviders = new List<IAssemblyProvider>(2) { provider1, provider2 };
        }

        /// <summary>
        ///     Initialises a new <see cref="CompositionAssemblyProvider" /> instance based on an enumerable of providers.
        /// </summary>
        /// <param name="providers">Enumerable of <see cref="IAssemblyProvider" /> instances.</param>
        public CompositionAssemblyProvider([NotNull] [ItemNotNull] IEnumerable<IAssemblyProvider> providers)
        {
            if (providers == null) {
                throw new ArgumentNullException(nameof(providers));
            }

            AssemblyProviders = providers.ToList();
        }

        /// <inheritdoc />
        [NotNull]
        public ReadOnlyCollection<Assembly> GetCandidateAssemblies()
        {
            var assemblies = AssemblyProviders
                .SelectMany(provider => provider.GetCandidateAssemblies())
                .Distinct()
                .ToList().AsReadOnly();

            if (assemblies != null) {
                return assemblies;
            }

            return new ReadOnlyCollection<Assembly>(new List<Assembly>(0));
        }

    }

}
