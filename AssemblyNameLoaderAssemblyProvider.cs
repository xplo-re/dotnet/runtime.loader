﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using JetBrains.Annotations;


namespace XploRe.Runtime.Loader
{

    /// <inheritdoc />
    /// <summary>
    ///     Base class for providers that operate on <see cref="T:System.Reflection.AssemblyName" />s.
    /// </summary>
    public abstract class AssemblyNameLoaderAssemblyProvider : IAssemblyProvider
    {

        /// <summary>
        ///     The <see cref="AssemblyLoadContext" /> used to load assemblies.
        /// </summary>
        [NotNull]
        public virtual AssemblyLoadContext AssemblyLoadContext { get; }

        /// <summary>
        ///     The <see cref="AssemblyName" />s collection.
        /// </summary>
        [NotNull]
        public abstract IReadOnlyCollection<AssemblyName> AssemblyNames { get; }

        /// <summary>
        ///     Initialises a new <see cref="AssemblyNameLoaderAssemblyProvider" /> instance with a given
        ///     <see cref="AssemblyLoadContext" /> instance.
        /// </summary>
        /// <param name="assemblyLoadContext">
        ///     The <see cref="T:System.Runtime.Loader.AssemblyLoadContext" /> used to load assemblies. If not provided
        ///     or <c>null</c>, the <see cref="P:System.Runtime.Loader.AssemblyLoadContext.Default" /> context instance
        ///     is used instead.
        /// </param>
        protected AssemblyNameLoaderAssemblyProvider(AssemblyLoadContext assemblyLoadContext = null)
        {
            if (assemblyLoadContext == null) {
                assemblyLoadContext = AssemblyLoadContext.Default;

                if (assemblyLoadContext == null) {
                    throw new RuntimeInconsistencyException(
                        "{0}.{1} is null.".FormatWith(typeof(AssemblyLoadContext), nameof(AssemblyLoadContext.Default))
                    );
                }
            }

            AssemblyLoadContext = assemblyLoadContext;
        }

        /// <inheritdoc />
        [NotNull]
        public virtual ReadOnlyCollection<Assembly> GetCandidateAssemblies()
        {
            var assemblies = AssemblyNames
                .Select(LoadFromAssemblyName)
                .Where(assembly => assembly != null)
                .ToList().AsReadOnly();

            if (assemblies != null) {
                return assemblies;
            }

            return new ReadOnlyCollection<Assembly>(new List<Assembly>(0));
        }

        /// <summary>
        ///     Loads an assembly by name using the configured <see cref="AssemblyLoadContext" /> instance.
        /// </summary>
        /// <param name="assemblyName">
        ///     An <see cref="AssemblyName" /> instance that identifies the assembly to load.
        /// </param>
        /// <returns>Loaded <see cref="Assembly" /> instance on success, otherwise <c>null</c>.</returns>
        [CanBeNull]
        protected virtual Assembly LoadFromAssemblyName([NotNull] AssemblyName assemblyName)
        {
            if (assemblyName == null) {
                throw new ArgumentNullException(nameof(assemblyName));
            }

            try {
                return AssemblyLoadContext.LoadFromAssemblyName(assemblyName);
            }
            catch (FileNotFoundException) {
            }

            return null;
        }

    }

}
