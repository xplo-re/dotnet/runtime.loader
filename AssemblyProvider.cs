﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;


namespace XploRe.Runtime.Loader
{

    /// <inheritdoc />
    /// <summary>
    ///     Simple <see cref="T:XploRe.Reflection.Loader.IAssemblyProvider" /> that yields a static collection of
    ///     <see cref="Assembly" /> instances provided during initialisation.
    /// </summary>
    public class AssemblyProvider : IAssemblyProvider
    {

        /// <summary>
        ///     Collection of <see cref="Assembly" /> instances the provider was initialised with.
        /// </summary>
        [NotNull]
        [ItemNotNull]
        public ReadOnlyCollection<Assembly> Assemblies { get; }

        /// <summary>
        ///     Initialises a new <see cref="TypeAssemblyProvider" /> instance with the <see cref="Assembly" /> instance
        ///     of a given <see cref="Type" /> instance.
        /// </summary>
        /// <param name="assemblies">Collection of <see cref="Assembly" /> instances to initialise provider with.</param>
        public AssemblyProvider([NotNull] [ItemNotNull] IEnumerable<Assembly> assemblies)
        {
            if (assemblies == null) {
                throw new ArgumentNullException(nameof(assemblies));
            }

            Assemblies = new ReadOnlyCollection<Assembly>(assemblies.ToList());
        }

        /// <inheritdoc />
        [NotNull]
        public ReadOnlyCollection<Assembly> GetCandidateAssemblies()
        {
            return Assemblies;
        }

    }

}
