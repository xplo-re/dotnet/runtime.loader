/*
 * xplo.re XploRe.NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using JetBrains.Annotations;
using Microsoft.Extensions.FileProviders;


namespace XploRe.Runtime.Loader
{

    /// <inheritdoc />
    /// <summary>
    ///     Provides assemblies from a given <see cref="T:Microsoft.Extensions.FileProviders.IFileProvider" /> instance.
    /// </summary>
    public class FileAssemblyProvider : IAssemblyProvider
    {

        /// <summary>
        ///     The <see cref="AssemblyLoadContext" /> instance used to load assemblies.
        /// </summary>
        [NotNull]
        public AssemblyLoadContext AssemblyLoadContext { get; }

        /// <summary>
        ///     The <see cref="IFileProvider" /> instance used by the assembly provider to find matching class libraries.
        /// </summary>
        [NotNull]
        protected IFileProvider FileProvider { get; }

        /// <summary>
        ///     Returns the default <see cref="FileAssemblyProvider" /> instance for assemblies found in the base
        ///     directory of the <see cref="AppContext" />.
        /// </summary>
        [NotNull]
        public static FileAssemblyProvider Default =>
            new FileAssemblyProvider(new PhysicalFileProvider(AppContext.BaseDirectory));

        /// <summary>
        ///     Initialises a new <see cref="FileAssemblyProvider" /> instance with a given <see cref="IFileProvider" />
        ///     instance.
        /// </summary>
        /// <param name="fileProvider">
        ///     The <see cref="IFileProvider" /> instance used to find matching class libraries.
        /// </param>
        /// <param name="assemblyLoadContext">
        ///     The <see cref="T:System.Runtime.Loader.AssemblyLoadContext" /> used to load assemblies. If not provided
        ///     or <c>null</c>, the <see cref="P:System.Runtime.Loader.AssemblyLoadContext.Default" /> context instance
        ///     is used instead.
        /// </param>
        public FileAssemblyProvider(
            [NotNull] IFileProvider fileProvider,
            AssemblyLoadContext assemblyLoadContext = null)
        {
            if (fileProvider == null) {
                throw new ArgumentNullException(nameof(fileProvider));
            }

            if (assemblyLoadContext == null) {
                assemblyLoadContext = AssemblyLoadContext.Default;

                if (assemblyLoadContext == null) {
                    throw new RuntimeInconsistencyException(
                        "{0}.{1} is null.".FormatWith(typeof(AssemblyLoadContext), nameof(AssemblyLoadContext.Default))
                    );
                }
            }

            FileProvider = fileProvider;
            AssemblyLoadContext = assemblyLoadContext;
        }

        /// <inheritdoc />
        [NotNull]
        public virtual ReadOnlyCollection<Assembly> GetCandidateAssemblies()
        {
            // Get directory contents. An empty directory name equals root folder of provider.
            var directory = FileProvider.GetDirectoryContents(string.Empty);

            if (directory == null) {
                throw new RuntimeInconsistencyException(
                    $"{0}.{1}() returned null".FormatWith(
                        nameof(FileProvider),
                        nameof(FileProvider.GetDirectoryContents)
                    )
                );
            }

            var assemblies = directory.Where(IsCandidateClassLibraryFile)
                                      .Select(Load)
                                      .Where(assembly => assembly != null)
                                      .ToList().AsReadOnly();

            if (assemblies != null) {
                return assemblies;
            }

            return new ReadOnlyCollection<Assembly>(new List<Assembly>(0));
        }

        /// <summary>
        ///     Tests whether the provided <see cref="IFileInfo" /> instance may reference a class library candidate.
        /// </summary>
        /// <param name="path">The <see cref="IFileInfo" /> instance to test.</param>
        /// <returns>
        ///     <c>true</c> if the provided <paramref name="path" /> may reference a class library candidate, otherwise 
        ///     <c>false</c>.
        /// </returns>
        protected virtual bool IsCandidateClassLibraryFile(IFileInfo path)
        {
            return path != null && path.Exists && !path.IsDirectory &&
                   path.Name?.EndsWithIgnoreCase(".dll", false) == true;
        }

        /// <summary>
        ///     Tries to load the class library from the given <see cref="IFileInfo" /> instance. 
        /// </summary>
        /// <param name="path">The <see cref="IFileInfo" /> instance of a class library to load.</param>
        /// <returns>An <see cref="Assembly" /> instance on success, otherwise <c>null</c>.</returns>
        protected virtual Assembly Load(IFileInfo path)
        {
            var physicalPath = path?.PhysicalPath;

            if (physicalPath == null) {
                return null;
            }

            return AssemblyLoadContext.LoadFromAssemblyNameUsingPath(physicalPath);
        }

    }

}
