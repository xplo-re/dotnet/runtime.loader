﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.IO;
using System.Reflection;
using System.Runtime.Loader;
using JetBrains.Annotations;


namespace XploRe.Runtime.Loader
{

    /// <summary>
    ///     Extends <see cref="AssemblyLoadContext" /> instances with extended loader methods.
    /// </summary>
    public static class AssemblyLoadContextExtensions
    {

        /// <summary>
        ///     Loads an assembly by first retrieving it's <see cref="AssemblyName" /> from the given path. If the
        ///     assembly has not already been loaded into the context and cannot be resolved by name, the assembly is
        ///     then directly loaded from the provided path. This avoids loading an assembly of the same name again if
        ///     it has already been loaded before.
        /// </summary>
        /// <remarks>
        ///     Note, that the returned assembly may be a different assembly using the same name as the assembly
        ///     specified by the given path.
        /// </remarks>
        /// <param name="assemblyLoadContext">
        ///     The <see cref="AssemblyLoadContext" /> instance used to load the assembly.
        /// </param>
        /// <param name="path">
        ///     Path of the assembly file to load if an assembly of the same name is not yet loaded.
        /// </param>
        /// <returns>The <see cref="Assembly" /> instance on success, otherwise <c>null</c>.</returns>
        public static Assembly LoadFromAssemblyNameUsingPath(
            [NotNull] this AssemblyLoadContext assemblyLoadContext,
            [NotNull] string path)
        {
            if (assemblyLoadContext == null) {
                throw new ArgumentNullException(nameof(assemblyLoadContext));
            }

            if (path == null) {
                throw new ArgumentNullException(nameof(path));
            }

            Assembly assembly = null;
            var assemblyName = AssemblyLoadContext.GetAssemblyName(path);

            try {
                // First try to load assembly by name. This will also fetch the assembly if it has already been loaded.
                assembly = assemblyLoadContext.LoadFromAssemblyName(assemblyName);
            }
            catch (FileNotFoundException) {
                try {
                    // Assembly has not been loaded. Explicitly load it from the file path.
                    assembly = assemblyLoadContext.LoadFromAssemblyPath(path);
                }
                catch (FileLoadException) {
                    // Not an assembly or assembly cannot be loaded. Ignore.
                }
            }

            return assembly;
        }

    }

}
