﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using JetBrains.Annotations;


namespace XploRe.Runtime.Loader
{

    /// <inheritdoc />
    /// <summary>
    ///     Provides all assemblies referenced by a given <see cref="T:System.Reflection.Assembly" /> instance.
    /// </summary>
    public class ReferencedAssemblyProvider : AssemblyNameLoaderAssemblyProvider
    {

        /// <inheritdoc />
        /// <summary>
        ///     The <see cref="T:System.Reflection.AssemblyName" /> instances referenced by <see cref="P:XploRe.Runtime.Loader.ReferencedAssemblyProvider.ReferencingAssembly" />.
        /// </summary>
        public override IReadOnlyCollection<AssemblyName> AssemblyNames =>
            ReferencingAssembly.GetReferencedAssemblies()?.ToList().AsReadOnly()
            ?? new ReadOnlyCollection<AssemblyName>(new List<AssemblyName>(0));

        /// <summary>
        ///     The <see cref="Assembly" /> instance whose referenced assemblies are being provided.
        /// </summary>
        [NotNull]
        public Assembly ReferencingAssembly { get; }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.Loader.ReferencedAssemblyProvider" /> instance with a
        ///     given reference <see cref="T:System.Reflection.Assembly" /> instance.
        /// </summary>
        /// <param name="referencingAssembly">
        ///     The <see cref="T:System.Reflection.Assembly" /> instance whose referened assemblies are being provided.
        /// </param>
        /// <param name="assemblyLoadContext">
        ///     The <see cref="T:System.Runtime.Loader.AssemblyLoadContext" /> used to load assemblies. If not provided
        ///     or <c>null</c>, the <see cref="P:System.Runtime.Loader.AssemblyLoadContext.Default" /> context instance
        ///     is used instead.
        /// </param>
        public ReferencedAssemblyProvider(
            [NotNull] Assembly referencingAssembly,
            AssemblyLoadContext assemblyLoadContext = null)
            : base(assemblyLoadContext)
        {
            if (referencingAssembly == null) {
                throw new ArgumentNullException(nameof(referencingAssembly));
            }

            ReferencingAssembly = referencingAssembly;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.Loader.ReferencedAssemblyProvider" /> instance with a
        ///     given type whose defining <see cref="T:System.Reflection.Assembly" /> instance is used as the reference.
        /// </summary>
        /// <param name="referencingType">
        ///     The <see cref="T:System.Type" /> instance whose defining <see cref="T:System.Reflection.Assembly" /> 
        ///     instance's referenced assemblies are being provided.
        /// </param>
        /// <param name="assemblyLoadContext">
        ///     The <see cref="T:System.Runtime.Loader.AssemblyLoadContext" /> used to load assemblies. If not provided
        ///     or <c>null</c>, the <see cref="P:System.Runtime.Loader.AssemblyLoadContext.Default" /> context instance
        ///     is used instead.
        /// </param>
        public ReferencedAssemblyProvider(
            [NotNull] Type referencingType,
            AssemblyLoadContext assemblyLoadContext = null) : base(assemblyLoadContext)
        {
            if (referencingType == null) {
                throw new ArgumentNullException(nameof(referencingType));
            }

            var typeInfo = referencingType.GetTypeInfo();

            if (typeInfo == null) {
                throw new ArgumentException("Provided type is not reflectable.", nameof(referencingType));
            }

            var assembly = typeInfo.Assembly;

            if (assembly == null) {
                throw new ArgumentException("Cannot get assembly from provided type.", nameof(referencingType));
            }

            ReferencingAssembly = assembly;
        }

    }

}
