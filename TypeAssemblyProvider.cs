﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using JetBrains.Annotations;


namespace XploRe.Runtime.Loader
{

    /// <inheritdoc />
    /// <summary>
    ///     Provides the assembly instance of a given type provided at the moment of initialisation.
    /// </summary>
    public class TypeAssemblyProvider : IAssemblyProvider
    {

        /// <summary>
        ///     <see cref="Assembly" /> instance of type provided during initialisation.
        /// </summary>
        [NotNull]
        public Assembly TypeAssembly { get; }

        /// <summary>
        ///     Initialises a new <see cref="TypeAssemblyProvider" /> instance with the <see cref="Assembly" /> instance
        ///     of a given <see cref="Type" />.
        /// </summary>
        /// <param name="type">
        ///     The <see cref="Type" /> instance to get the <see cref="Assembly" /> instance from. The type must be 
        ///     reflectable, i.e. implement the <see cref="IReflectableType" /> interface, otherwise an 
        ///     <see cref="ArgumentException" /> is thrown.
        /// </param>
        public TypeAssemblyProvider([NotNull] Type type)
        {
            if (type == null) {
                throw new ArgumentNullException(nameof(type));
            }

            var typeInfo = type.GetTypeInfo();

            if (typeInfo == null) {
                throw new ArgumentException("Provided type is not reflectable.", nameof(type));
            }

            var assembly = typeInfo.Assembly;

            if (assembly == null) {
                throw new ArgumentException("Cannot get assembly from provided type.", nameof(type));
            }

            TypeAssembly = assembly;
        }

        /// <inheritdoc />
        [NotNull]
        public ReadOnlyCollection<Assembly> GetCandidateAssemblies()
        {
            return new ReadOnlyCollection<Assembly>(new List<Assembly>(1) { TypeAssembly });
        }

    }

}
