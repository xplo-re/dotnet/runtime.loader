﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.ObjectModel;
using System.Reflection;


namespace XploRe.Runtime.Loader
{

    /// <summary>
    ///     An assembly provider retrieves a collection of <see cref="Assembly" /> candidates. The consumer should
    ///     further filter out candidates as needed. 
    /// </summary>
    public interface IAssemblyProvider
    {

        // ReSharper disable once ReturnTypeCanBeEnumerable.Global
        /// <summary>
        ///     Returns a collection of assembly candidates.
        /// </summary>
        /// <returns>
        ///     Collection of candidate <see cref="Assembly" /> instances. The returned collection may be cached by the
        ///     provider. 
        /// </returns>
        ReadOnlyCollection<Assembly> GetCandidateAssemblies();

    }

}
