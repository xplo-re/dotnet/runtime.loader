﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using JetBrains.Annotations;


namespace XploRe.Runtime.Loader
{

    /// <inheritdoc />
    /// <summary>
    ///     Provides all <see cref="Assembly" /> instances identified by a collection of <see cref="AssemblyName" />s.
    /// </summary>
    public class AssemblyNameCollectionAssemblyProvider : AssemblyNameLoaderAssemblyProvider
    {

        /// <inheritdoc />
        /// <summary>
        ///     The <see cref="T:System.Reflection.AssemblyName" />s collection this provider was initialised with.
        /// </summary>
        public override IReadOnlyCollection<AssemblyName> AssemblyNames { get; }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.Loader.AssemblyNameCollectionAssemblyProvider" />
        ///     instance with a given collection of <see cref="T:System.Reflection.AssemblyName" />s.
        /// </summary>
        /// <param name="assemblyNames">
        ///     The <see cref="T:System.Reflection.AssemblyName" />s collection that is provided by this instance.
        /// </param>
        /// <param name="assemblyLoadContext">
        ///     The <see cref="T:System.Runtime.Loader.AssemblyLoadContext" /> used to load assemblies. If not provided
        ///     or <c>null</c>, the <see cref="P:System.Runtime.Loader.AssemblyLoadContext.Default" /> context instance
        ///     is used instead.
        /// </param>
        public AssemblyNameCollectionAssemblyProvider(
            [NotNull] [ItemNotNull] IEnumerable<AssemblyName> assemblyNames,
            AssemblyLoadContext assemblyLoadContext = null)
            : base(assemblyLoadContext)
        {
            if (assemblyNames == null) {
                throw new ArgumentNullException(nameof(assemblyNames));
            }

            AssemblyNames = new ReadOnlyCollection<AssemblyName>(assemblyNames.ToList());
        }

    }

}
