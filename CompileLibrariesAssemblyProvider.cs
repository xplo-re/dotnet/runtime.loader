﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyModel;


namespace XploRe.Runtime.Loader
{

    // The default dependency context that is used by this provider is only available in .NETStandard 1.6 and newer.
#if !NETSTANDARD1_5

    /// <inheritdoc />
    /// <summary>
    ///     Provides assemblies for a collection of
    ///     <see cref="T:Microsoft.Extensions.DependencyModel.CompilationLibrary" /> instances.
    /// </summary>
    /// <remarks>
    ///     This provider is only available for .NET Standard 1.6 or newer.
    /// </remarks>
    public class CompileLibrariesAssemblyProvider : AssemblyNameCollectionAssemblyProvider
    {

        /// <summary>
        ///     Provides assemblies for all compilation libraries of the default <see cref="DependencyContext" />.
        /// </summary>
        /// <remarks>
        ///     As the compile libraries are retrieved from the default <see cref="DependencyContext" />, compile
        ///     libraries are only available if the ".deps.json" dependency information file for the entry assembly
        ///     returned by <see cref="Assembly.GetEntryAssembly" /> is available. Otherwise, the list of compile
        ///     libraries is empty. 
        /// </remarks>
        [NotNull]
        public static CompileLibrariesAssemblyProvider Default
        {
            get {
                var libraries = DependencyContext.Default?.CompileLibraries ?? new List<CompilationLibrary>(0);

                return new CompileLibrariesAssemblyProvider(libraries);
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.Loader.CompileLibrariesAssemblyProvider" /> instance
        ///     with a given collection of <see cref="T:Microsoft.Extensions.DependencyModel.CompilationLibrary" />
        ///     instances.
        /// </summary>
        /// <param name="compilationLibraries">
        ///     The collection of <see cref="T:Microsoft.Extensions.DependencyModel.CompilationLibrary" /> instances to
        ///     initialise the provider with.
        /// </param>
        /// <param name="assemblyLoadContext">
        ///     The <see cref="T:System.Runtime.Loader.AssemblyLoadContext" /> used to load assemblies. If not provided
        ///     or <c>null</c>, the <see cref="P:System.Runtime.Loader.AssemblyLoadContext.Default" /> context instance
        ///     is used instead.
        /// </param>
        public CompileLibrariesAssemblyProvider(
            [NotNull] [ItemNotNull] IEnumerable<CompilationLibrary> compilationLibraries,
            AssemblyLoadContext assemblyLoadContext = null)
            : base(compilationLibraries.Select(library => new AssemblyName(library.Name)), assemblyLoadContext)
        {
        }

    }

#endif

}
